package odf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.eclipse.jdt.annotation.Nullable;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.thaiopensource.util.PropertyMapBuilder;
import com.thaiopensource.validate.SchemaReader;
import com.thaiopensource.validate.ValidateProperty;
import com.thaiopensource.validate.ValidationDriver;
import com.thaiopensource.validate.prop.rng.RngProperty;
import com.thaiopensource.validate.rng.SAXSchemaReader;
import com.thaiopensource.xml.sax.ErrorHandlerImpl;

public class OdfInfo {

	private static @Nullable OdfRngInfo odfRngInfo;
	private static @Nullable OdfRngInfo manifestRngInfo;
	private static @Nullable OdfRngInfo rngRngInfo;

	final FileType fileType;
	private @Nullable ValidationDriver driver12;
	private @Nullable ValidationDriver driver11;
	final ErrorHandler errorHandler;
	String version;

	static ValidationDriver load(String path, ErrorHandler errorHandler) {
		SchemaReader schemaReader = SAXSchemaReader.getInstance();
		PropertyMapBuilder properties = new PropertyMapBuilder();
		properties.put(ValidateProperty.ERROR_HANDLER, errorHandler);
		RngProperty.CHECK_ID_IDREF.add(properties);
		properties.put(RngProperty.CHECK_ID_IDREF, null);
		ValidationDriver driver = new ValidationDriver(
				properties.toPropertyMap(), schemaReader);

		InputSource in = ValidationDriver.uriOrFileInputSource(path);
		try {
			driver.loadSchema(in);
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		return driver;
	}

	OdfInfo(FileType fileType) {
		this.fileType = fileType;
		errorHandler = new ErrorHandler();
		version = "1.2";
	}

	enum FileType {
		RNG, ODF, ODFMANIFEST
	}

	public OdfRngInfo getRngInfo(XML xml) throws FileNotFoundException,
			SAXException, IOException, TransformerException {
		synchronized (OdfInfo.class) {
			if (fileType == FileType.ODFMANIFEST) {
				if (manifestRngInfo != null) {
					return manifestRngInfo;
				}
				manifestRngInfo = loadInfo(
						"src/rng/OpenDocument-v1.2-os-manifest-schema.rng", xml);
				return manifestRngInfo;
			} else if (fileType == FileType.ODF) {
				if (odfRngInfo != null) {
					return odfRngInfo;
				}
				odfRngInfo = loadInfo(
						"src/rng/OpenDocument-v1.2-os-schema.rng", xml);
				return odfRngInfo;
			} else if (fileType == FileType.RNG) {
				rngRngInfo = loadInfo("src/rng/relaxng.rng", xml);
				return rngRngInfo;
			} else {
				throw new RuntimeException();
			}
		}
	}

	private OdfRngInfo loadInfo(String rngPath, XML xml)
			throws FileNotFoundException, SAXException, IOException,
			TransformerException {
		Document grammar = xml.parse(new FileInputStream(new File(rngPath)),
				null, null);
		grammar.setDocumentURI(rngPath);
		return new OdfRngInfo(grammar, xml);
	}

	public ValidationDriver getDriver11() {
		if (driver11 != null) {
			return driver11;
		} else if (fileType == FileType.ODFMANIFEST) {
			driver11 = load("src/rng/OpenDocument-manifest-schema-v1.1.rng",
					errorHandler);
			return driver11;
		} else if (fileType == FileType.ODF) {
			driver11 = load(
					"src/rng/OpenDocument-schema-v1.1-errata01-complete.rng",
					errorHandler);
			return driver11;
		} else {
			driver11 = load("src/rng/relaxng.rng", errorHandler);
			return driver11;
		}
	}

	public ValidationDriver getDriver12() {
		if (driver12 != null) {
			return driver12;
		} else if (fileType == FileType.ODFMANIFEST) {
			driver12 = load("src/rng/OpenDocument-v1.2-os-manifest-schema.rng",
					errorHandler);
			return driver12;
		} else if (fileType == FileType.ODF) {
			driver12 = load("src/rng/OpenDocument-v1.2-os-schema.rng",
					errorHandler);
			return driver12;
		} else {
			driver12 = load("src/rng/relaxng.rng", errorHandler);
			return driver12;
		}
	}
}

class ErrorHandler extends ErrorHandlerImpl {
	final List<SAXParseException> errors = new LinkedList<SAXParseException>();

	ErrorHandler() {
		super();
	}

	@Override
	public void error(@Nullable SAXParseException e) {
		errors.add(e);
	}

	@Override
	public void fatalError(@Nullable SAXParseException e)
			throws SAXParseException {
		errors.add(e);
	}

	public void reset() {
		errors.clear();
	}
}