package odf;

import java.util.HashMap;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class RngNormalizer {

	static void normalize(Document rng) {
		// merge defines with the same name and sort them by name attribute
		final HashMap<String, Element> defs = OdfRngInfoParser
				.getDefinitions(rng);
		final TreeSet<String> names = new TreeSet<String>();
		names.addAll(defs.keySet());
		final Element grammar = rng.getDocumentElement();
		for (String name : names) {
			final Element def = defs.get(name);
			def.removeAttribute("combine");

			// move non-element nodes that precede this <define/> and the
			// <define/> to the end of the <grammar/> element
			Node before = def.getPreviousSibling();
			while (before != null && before.getNodeType() != Node.ELEMENT_NODE) {
				before = before.getPreviousSibling();
			}
			if (before != null) {
				before = before.getNextSibling();
			}
			while (before != null && before != def) {
				final Node n = before;
				before = before.getNextSibling();
				grammar.appendChild(n);
			}
			grammar.appendChild(def);
		}
	}
}
