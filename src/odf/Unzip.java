package odf;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {
	static public void unzip(Path zipfile, Path destinationDir)
			throws IOException {
		ZipInputStream zip = new ZipInputStream(new FileInputStream(
				zipfile.toFile()));
		ZipEntry entry = zip.getNextEntry();
		byte[] bytesIn = new byte[128 * 64];
		while (entry != null) {
			Path target = destinationDir.resolve(entry.getName());
			if (entry.isDirectory()) {
				Files.createDirectories(target);
			} else {
				Files.createDirectories(target.getParent());
				FileOutputStream bos = new FileOutputStream(target.toFile());
				int read = 0;
				while ((read = zip.read(bytesIn)) != -1) {
					bos.write(bytesIn, 0, read);
				}
				bos.close();
			}
			zip.closeEntry();
			entry = zip.getNextEntry();
		}
		zip.close();
	}
}
