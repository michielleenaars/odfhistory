package odf;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.XMLFilterImpl;

// clean odf while parsing
class OdfCleaner extends XMLFilterImpl {

	final String version;

	OdfCleaner(String version) {
		this.version = version;
	}

	@Override
	public void startElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName, @Nullable Attributes attributes)
			throws SAXException {
		if (uri == null || localName == null || qName == null
				|| attributes == null) {
			throw new NullPointerException();
		}

		AttributesImpl atts = new AttributesImpl(attributes);
		int l = atts.getLength();
		for (int i = 0; i < l; ++i) {
		}
		super.startElement(uri, localName, qName, atts);
	}

	@Override
	public void endElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName) throws SAXException {
		super.endElement(uri, localName, qName);
	}

	@Override
	public void characters(@Nullable char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
	}

	@Override
	public void ignorableWhitespace(@Nullable char[] ch, int start, int length)
			throws SAXException {
	}

}
