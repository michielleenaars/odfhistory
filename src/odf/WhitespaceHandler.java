package odf;

import java.util.HashSet;
import java.util.Stack;

import org.eclipse.jdt.annotation.Nullable;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.XMLFilterImpl;

class WhitespaceHandler extends XMLFilterImpl {

	final Stack<QName> stack = new Stack<QName>();

	final HashSet<QName> elementsWithNoText;

	WhitespaceHandler(@Nullable OdfRngInfo info) {
		elementsWithNoText = (info == null) ? new HashSet<QName>() : info
				.getElementsWithNoText();
	}

	void reset() {
		stack.clear();
	}

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		stack.clear();
	}

	@Override
	public void startElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName, @Nullable Attributes atts)
			throws SAXException {
		if (localName == null) {
			throw new RuntimeException();
		}
		stack.add(QName.qname(uri, localName));
		super.startElement(uri, localName, qName, atts);
	}

	@Override
	public void endElement(@Nullable String uri, @Nullable String localName,
			@Nullable String qName) throws SAXException {
		stack.pop();
		super.endElement(uri, localName, qName);
	}

	@Override
	public void characters(@Nullable char[] ch, int start, int length)
			throws SAXException {
		if (!elementsWithNoText.contains(stack.lastElement())) {
			super.characters(ch, start, length);
		}
	}

	@Override
	public void ignorableWhitespace(@Nullable char[] ch, int start, int length)
			throws SAXException {
	}
}
